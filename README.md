# Belajar Git Workflow dan Continuous Deployment dengan Gitlab CI #

Requirement:

* Code Review
* Staged Deployment : development, testing, production
* Continuous Delivery
* Parallel development (bugfix and feature)
* Support maintenance for multiple version (`1.x` and `2.x`)

Use Case :

* Develop Feature
* Integrate changes
* Deploy

  * to development
  * to testing
  * to production

* Fix bug in production
* Maintenance branch

Alternatif solusi:

* [Linux Kernel Development](https://www.kernel.org/pub/software/scm/git/docs/gitworkflows.html)
* [Git Flow](http://nvie.com/posts/a-successful-git-branching-model/)
* [Github Flow](http://scottchacon.com/2011/08/31/github-flow.html)
* [Gitlab Flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)
* [Atlassian Flow](https://www.atlassian.com/continuous-delivery/continuous-delivery-workflows-with-feature-branching-and-gitflow)

# Workflow ArtiVisi #

Garis besar:

* single permanent branch : `master`
* develop di topic branch
* bugfix di hotfix branch
* push branch ke remote
* raise pull request dari branch
* merge pull request
* hapus branch setelah merge
* tag
* deploy ketika ada tag dibuat

    * A.B.C-M.xxx : deploy ke dev (otomatis)
    * A.B.C-RC.xxx : deploy ke test (otomatis)
    * A.B.C-RELEASE : deploy ke production (manual)

Ada dua jenis workflow : untuk closed source (menggunakan private repo) dan open source (menggunakan public repo). Secara garis besar flownya mirip, perbedaannya hanya sebagai berikut:

* Project closed source menggunakan satu repo yang diakses semua orang. Tetapi yang boleh push ke master cuma `integrator`. `Kontributor` hanya boleh push ke topic branch.
* Project open source memiliki satu repo utama yang bisa dibaca semua orang, tapi hanya bisa ditulis (push) oleh `integrator`. `Kontributor` membuat fork ke reponya sendiri, kemudian push ke repo mereka tersebut. Selanjutnya, `integrator` akan pull dari repo `kontributor` begitu mendapatkan pull request.

## Closed Source Project ##

Menggunakan private repo. Yang boleh merge ke master hanya integrator.

### Normal Development ###

Kontributor

* git pull `origin` `master`
* git checkout -b `fitur-1`
* git commit
* git push `origin` `fitur-1`
* raise pull request

Integrator

* git checkout -b `integrasi-fitur-1`
* git pull `origin` `fitur-1`
* test/review ok
* git checkout `master`
* git pull `origin` `master`
* git merge `integrasi-fitur-1`
* git push `origin` `master`
* git tag `1.0.0-M.001` --> otomatis deploy ke development
* git branch -d `integrasi-fitur-1`
* git push `origin` `:fitur-1` -> hapus branch fitur-1 di `origin`

Kontributor

* git checkout `master`
* git pull `origin` `master`
* git branch -d `fitur-1`

Internal Tester

* Functional Test
* Stress Test
* Report Bug / Report OK

Integrator

* git tag `1.0.0-RC.001` --> otomatis deploy ke testing

Client Tester

* UAT
* Security Test
* Report Bug / Report OK

Integrator

*  git tag `1.0.0-RELEASE` --> manual deploy ke canary/production

### Bug in Production 1.0.0-RELEASE ###

Kontributor

* git checkout -b `fix-1` `1.0.0-RELEASE`
* git commit
* git push `origin` `fix-1`
* raise pull request

Integrator

* git remote update
* git checkout -b `integrasi-fix-1` `1.0.0-RELEASE`
* git pull `origin` `fix-1`
* test/review ok
* git tag `1.0.1-M.001` --> otomatis deploy ke development
* git push --tags

Internal Tester

* test
* report bug/ok

Integrator

* git tag `1.0.1-RC.001` --> otomatis deploy ke testing
* git push --tags

Client Tester

* test
* report bug/ok

Integrator

* git tag `1.0.1-RELEASE` --> otomatis/manual deploy ke production
* git push --tags
* git checkout `master`
* git pull `origin` `master`
* git merge `integrasi-fix-1`
* git push `origin` `master`
* git branch -d `integrasi-fix-1`
* git push `origin` `:fix-1` --> hapus branch fitur-1 di `origin`

Kontributor

* git pull `origin` `master`
* git branch -d `fix-1`


## Open Source Development ##

Tiap kontributor fork dari repo utama ke repo pribadi masing-masing.

Kontributor

* Fork dari `upstream`
* Clone
* git remote add `upstream` `URL_REPO_UPSTREAM`
* git commit -m "kontribusi-1"
* git push `origin` `master`
* raise pull request

Integrator

* git checkout -b `integrasi-kontribusi-1`
* bila PR terdiri dari > 1 commit :

    * tarik semua : git pull `URL-REPO` `BRANCH-PR`
    * tarik commit tertentu saja : curl `URL-COMMIT.patch` | git am

* review/test ok
* git checkout `master`
* git merge `integrasi-kontribusi-1`
* git push `origin` `master`
* git branch -d `integrasi-kontribusi-1`

Kontributor

* git pull `upstream` `master`

Flow deployment sama dengan closed source. Integrator membuat tag ketika mau deploy. Flow hotfix juga mirip dengan closed source, bedanya cuma kalau open source tiap orang pakai remote repo sendiri-sendiri, bukan shared repo (satu repo dipakai semua kontributor).

### Gitlab CI ###

[Sample File Konfigurasi](.gitlab-ci.yml)

## Caveat ##

* `master` tidak deployable-to-production
* kualitas deployment ditentukan oleh tag

Referensi:

* https://www.kernel.org/pub/software/scm/git/docs/gitworkflows.html
* http://nvie.com/posts/a-successful-git-branching-model/
* http://scottchacon.com/2011/08/31/github-flow.html
* https://about.gitlab.com/2014/09/29/gitlab-flow/
* https://docs.gitlab.com/ee/workflow/gitlab_flow.html
* https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/
* http://stackoverflow.com/a/43236779/855470
* http://semver.org/
* http://changelog.ca/log/2012/07/19/dark_launching_software_features
